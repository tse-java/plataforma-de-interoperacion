package uy.viruscontrol.pi.exception;

public class PIException extends Exception{
    public PIException(String s) {
        super(s);
    }
}
