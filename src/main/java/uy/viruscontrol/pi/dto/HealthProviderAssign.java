package uy.viruscontrol.pi.dto;

public class HealthProviderAssign {

    private String citizenDocumentNumber;

    private Long healthProvider;

    public HealthProviderAssign() {
    }

    public String getCitizenDocumentNumber() {
        return citizenDocumentNumber;
    }

    public void setCitizenDocumentNumber(String citizenDocumentNumber) {
        this.citizenDocumentNumber = citizenDocumentNumber;
    }

    public Long getHealthProvider() {
        return healthProvider;
    }

    public void setHealthProvider(Long healthProvider) {
        this.healthProvider = healthProvider;
    }
}
