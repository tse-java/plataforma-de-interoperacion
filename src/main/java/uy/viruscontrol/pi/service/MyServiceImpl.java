package uy.viruscontrol.pi.service;

import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.jeasy.random.FieldPredicates;
import org.jeasy.random.randomizers.FirstNameRandomizer;
import org.jeasy.random.randomizers.LastNameRandomizer;
import uy.viruscontrol.pi.dto.DTOCitizen;
import uy.viruscontrol.pi.dto.HealthProviderAssign;
import uy.viruscontrol.pi.util.Persistence;
import uy.viruscontrol.pi.util.Util;

import javax.jws.WebService;
import javax.xml.ws.soap.Addressing;
import java.util.List;
import java.util.logging.Logger;

@WebService(endpointInterface = "uy.viruscontrol.pi.service.MyService")
//@Addressing(enabled=true, required=true)
public class MyServiceImpl implements MyService {

    private static final Logger LOG = Logger.getLogger(MyServiceImpl.class.getName());
    private final Persistence persistence = Persistence.getInstance();

    @Override
    public DTOCitizen getCitizenInformation(String documentNumber) {
        LOG.info("Executing operation getCitizenInformation");
        LOG.info(documentNumber);
        DTOCitizen citizen = persistence.getCitizen(documentNumber);
        if(citizen == null){
            EasyRandom generator = new EasyRandom(new EasyRandomParameters()
                    .randomize(FieldPredicates.named("firstName"), new FirstNameRandomizer())
                    .randomize(FieldPredicates.named("lastName"), new LastNameRandomizer())
            );
            citizen = generator.nextObject(DTOCitizen.class);
            citizen.setDocumentNumber(documentNumber);
            citizen.setBirthDay(Util.getRandomDate());
            citizen.setNationality(Util.getRandomNationality());
            persistence.addCitizen(citizen);
        }
        return citizen;
    }

    @Override
    public Long getPacientHealthProvider(String documentNumber) {
        LOG.info("Executing operation getPacientHealthProvider");
        LOG.info(documentNumber);
        HealthProviderAssign assign = persistence.getHealthProviderAssign(documentNumber);
        if (assign == null) {
            assign = new HealthProviderAssign();
            assign.setCitizenDocumentNumber(documentNumber);
            assign.setHealthProvider(Util.getRandomHealthProvider());
            persistence.addAssignment(assign);
        }
        return assign.getHealthProvider();
    }

    @Override
    public void setHealthProviders(List<Long> ids) {
        LOG.info("Executing operation setHealthProviders");
        LOG.info(ids.toString());
        persistence.setHealthProviderList(ids);
    }


}
