package uy.viruscontrol.pi.service;


import uy.viruscontrol.pi.dto.DTOCitizen;
import uy.viruscontrol.pi.exception.PIException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.soap.Addressing;
import java.util.List;

@WebService
@Addressing(required=true)
public interface MyService {

    @WebMethod(operationName = "getCitizenInformation")
    DTOCitizen getCitizenInformation(String documentNumber);

    @WebMethod(operationName = "getPacientHealthProvider")
    Long getPacientHealthProvider(String documentNumber);

    @WebMethod(operationName = "setHealthProviders")
    void setHealthProviders(List<Long> ids);

}

