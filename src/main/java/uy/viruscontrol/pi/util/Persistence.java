package uy.viruscontrol.pi.util;

import uy.viruscontrol.pi.dto.DTOCitizen;
import uy.viruscontrol.pi.dto.HealthProviderAssign;

import java.util.ArrayList;
import java.util.List;

public class Persistence {

    private static Persistence instance;

    private List<DTOCitizen> citizenList = new ArrayList<>();

    private List<HealthProviderAssign> healthProviderAssignList = new ArrayList<>();

    private List<Long> HealthProviderList = new ArrayList<>();

    public Persistence() {}

    public static Persistence getInstance(){
        if (instance == null) {
            instance = new Persistence();
        }
        return instance;
    }

    public DTOCitizen getCitizen(String documentNumber){
        for (DTOCitizen dtoCitizen : citizenList) {
            if (dtoCitizen.getDocumentNumber().equals(documentNumber)){
                return dtoCitizen;
            }
        }
        return null;
    }

    public void addCitizen(DTOCitizen citizen){
        citizenList.add(citizen);
    }



    public void addAssignment(HealthProviderAssign providerAssign){
        healthProviderAssignList.add(providerAssign);
    }

    public HealthProviderAssign getHealthProviderAssign(String documentNumber){
        for (HealthProviderAssign healthProviderAssign : healthProviderAssignList) {
            if (healthProviderAssign.getCitizenDocumentNumber().equals(documentNumber)){
                return healthProviderAssign;
            }
        }
        return null;
    }

    public List<Long> getHealthProviderList() {
        return HealthProviderList;
    }

    public void setHealthProviderList(List<Long> healthProviderList) {
        HealthProviderList = healthProviderList;
    }
}
