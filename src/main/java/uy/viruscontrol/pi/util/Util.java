package uy.viruscontrol.pi.util;


import java.util.Calendar;
import java.util.Date;

public class Util {

    private static final Persistence persistence = Persistence.getInstance();

    private static final String[] nationalities = new String[]{"Uruguay", "Brasil", "Argentina", "Paraguay", "Bolivia", "Peru"};

    public static Date getRandomDate() {
        Date randomDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(randomDate);
        calendar.add(Calendar.HOUR, (int) (Math.random() * 2));
        calendar.add(Calendar.MINUTE, (int) (Math.random() * 45) + 15);
        calendar.add(Calendar.YEAR, -(int) (Math.random() * 100));
        calendar.set(Calendar.DAY_OF_MONTH, ((int) (Math.random() * 25)) + 1);
        calendar.set(Calendar.DAY_OF_MONTH, ((int) (Math.random() * 23)) + 1);
        return calendar.getTime();
    }

    public static String getRandomNationality() {
        return nationalities[(int) (Math.random() * 6)];
    }

    public static Long getRandomHealthProvider() {
        return persistence.getHealthProviderList().get((int) (Math.random() * persistence.getHealthProviderList().size()));
    }
}
